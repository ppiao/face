package com.yinuo.face.record;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.blankj.utilcode.util.SnackbarUtils;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.orhanobut.logger.Logger;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.yinuo.face.R;

import java.util.ArrayList;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class UserRecordListActivity extends AppCompatActivity {

    private final List<UserRecord> userList = new ArrayList<>();
    private Handler handler = new Handler();
    private UserRecordListAdapter adapter;
    private ModelAdapter<UserRecord> modelAdapter;
    private TwinklingRefreshLayout refreshLayout;
    private RefreshListenerAdapter refreshListener = new RefreshListenerAdapter() {
        @Override
        public void onRefresh(final TwinklingRefreshLayout refreshLayout) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    reload(0);
                    refreshLayout.finishRefreshing();
                }
            }, 500);
        }

        @Override
        public void onLoadMore(final TwinklingRefreshLayout refreshLayout) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    reload(userList.size());
                    refreshLayout.finishLoadmore();
                }
            }, 500);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        refreshLayout = findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(refreshListener);

        RecyclerView viewById = findViewById(R.id.listview);
        viewById.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserRecordListAdapter(R.layout.fragment_user_list_item, userList);
        viewById.setAdapter(adapter);
        adapter.openLoadAnimation();

        modelAdapter = FlowManager.getModelAdapter(UserRecord.class);

    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshLayout.startRefresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        if (item.getItemId() == R.id.delete_all) {
            final ACProgressFlower dialog = new ACProgressFlower.Builder(this).direction(ACProgressConstant.DIRECT_CLOCKWISE).themeColor(Color.WHITE).text("正在重新加载人脸").fadeColor(Color.DKGRAY).build();
            dialog.show();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    modelAdapter.deleteAll(userList);
                    userList.clear();
                    adapter.notifyDataSetChanged();
                    dialog.cancel();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.record_menu, menu);
        return true;
    }

    private void reload(int i) {
        if (i == 0) {
            userList.clear();
        }
        List<UserRecord> users = SQLite.select().from(UserRecord.class).offset(i).limit(30).queryList();
        Logger.d("加载 %d -> %d = %d ", i, 30, users.size());
        userList.addAll(users);
        adapter.notifyDataSetChanged();

        if (users.size() == 0) {
            SnackbarUtils.with(refreshLayout).setMessage("没有更多了").setBottomMargin(1).showWarning();
        }
    }

}
